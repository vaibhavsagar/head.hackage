#!/bin/sh

# The following throws a fontconfig error but it appears to be innocuous.
unflatten -f -l 3 \
  | dot \
  | neato -s -n2 -Tsvg -Gsize="40,10" -Gratio="fill" -Gsplines=true

#unflatten -f -l 2 | dot | sfdp -s -n2 -Tsvg -o output.svg -Gsize="24,12" -Gratio="fill" -Goverlap="false"
