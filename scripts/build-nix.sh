#!/bin/sh

# A convenient wrapper for build-all.nix.

if [ -z "$GHC_TARBALL" ]; then
  echo '$GHC_TARBALL is not set; please set it.'
  exit 1
fi

targets=$@
if [ -z "$targets" ]; then
  targets=testedPackages
fi

echo "Building $targets with GHC options \"$EXTRA_HC_OPTS\"..."

nix build -f scripts/build-all.nix --keep-going testedPackages \
    --arg bindistTarball $GHC_TARBALL
    --arg extraHcOpts \"$EXTRA_HC_OPTS\"

echo "Build finished. Running summarize..."
python3 scripts/summarize.py

echo
echo "Build finished. I produced the following:"
echo " * summary.json: a summary of the build"
echo " * summary.dot:  the dependency graph of packages I built"
echo " * logs/*:       the build logs"
echo
